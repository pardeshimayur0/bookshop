<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Welcome</title>
</head>
<body>
	<h4>Welcome, ${cust.name}</h4>
	<h5>You have logged in successfully.</h5>
	<br/>
	<h5>Your Mobile: ${cust.mobile}</h5>
	<h5>Your Email: ${cust.email}</h5>
	<h5>Your Address: ${requestScope.cust.address}</h5>
</body>
</html>
