<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cart</title>
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	Hello, ${cust.name} <a href="signout" class="right-align">Sign Out</a><hr/>
	<sf:form modelAttribute="book" action="updatebook">
	<table class="table">
		<thead>
			<tr>
				<td colspan="2" align="center">Edit Book</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Id:</td><td><sf:input path="id"/></td>
			</tr>
			<tr>
				<td>Name:</td><td><sf:input path="name"/></td>
			</tr>
			<tr>
				<td>Author:</td><td><sf:input path="author"/></td>
			</tr>
			<tr>
				<td>Subject:</td><td><sf:input path="subject"/></td>
			</tr>
			<tr>
				<td>Price:</td><td><sf:input path="price"/></td>
			</tr>
		</tbody>
		<!-- <tr><td><input type="button">Add new book</td></tr> -->
		<tr><td colspan="2" align="center"><input type="submit" value="Edit Book"/></td></tr>
	</table>
	</sf:form>
</body>
</html>

