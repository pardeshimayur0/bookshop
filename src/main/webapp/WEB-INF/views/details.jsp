<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Details</title>
</head>
<body>
	Id: ${b.id} <br/>
	Name: ${b.name} <br/>
	Author: ${b.author} <br/>
	Subject: ${b.subject} <br/>
	Price: ${b.price} <br/>
	
	<a href="books?subject=${b.subject}">Back</a>
</body>
</html>

