<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Books</title>
</head>
<body>
	Hello, ${cust.name} <hr/>
	<sf:form action="addcart">
		<%--<sf:checkboxes path="book" items="${bookList}" itemLabel="name" itemValue="id" delimiter="<br/>"/>--%>
		<c:forEach var="b" items="${bookList}">
			<sf:checkbox path="book" label="${b.name}" value="${b.id}"/>
			<a href="details?id=${b.id}">Details</a>
			<br/>
		</c:forEach>		
		<br/>
		<input type="submit" value="Add Cart">
	</sf:form>
</body>
</html>

