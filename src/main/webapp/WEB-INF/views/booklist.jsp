<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cart</title>
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	Hello, ${cust.name} <a href="signout" class="right-align">Sign Out</a><hr/>
	<c:set var="total" scope="page" value="0.0"/>
	<table class="table">
		<thead>
			<tr>
				<td>Id</td>
				<td>Name</td>
				<td>Author</td>
				<td>Subject</td>
				<td>Price</td>
				<td>Edit</td>
				<td>Delete</td>
			</tr>
		</thead>
		<c:forEach var="b" items="${bookList}">
			<tr>
				<td>${b.id}</td>
				<td>${b.name}</td>
				<td>${b.author}</td>
				<td>${b.subject}</td>
				<td>${b.price}</td>
				<td align="center"><a href="edit?id=${b.id}">edit</a></td>
				<td align="center"><a href="delete?id=${b.id}">del</a></td>
			</tr>
		</c:forEach>
		<!-- <tr><td><input type="button">Add new book</td></tr> -->
		<tr><td colspan="7" align="center"><a href="add">Add new book</a></td></tr>
	</table>
	
</body>
</html>

