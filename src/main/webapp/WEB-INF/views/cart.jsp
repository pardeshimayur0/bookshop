<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cart</title>
</head>
<body>
	Hello, ${cust.name} <hr/>
	<c:set var="total" scope="page" value="0.0"/>
	<table border="1">
		<thead>
			<tr>
				<td>Id</td>
				<td>Name</td>
				<td>Author</td>
				<td>Subject</td>
				<td>Price</td>
			</tr>
		</thead>
		<c:forEach var="b" items="${bookList}">
			<tr>
				<td>${b.id}</td>
				<td>${b.name}</td>
				<td>${b.author}</td>
				<td>${b.subject}</td>
				<td>${b.price}</td>
			</tr>
			<c:set var="total" scope="page" value="${total + b.price}"/>
		</c:forEach>
	</table>
	Total Bill: Rs. ${total}/- <br/><br/>
	
	<a href="signout">Sign Out</a>
</body>
</html>

