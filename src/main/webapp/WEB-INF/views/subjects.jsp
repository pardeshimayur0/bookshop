<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Subjects</title>
</head>
<body>
	<%--
	<c:forEach var="subj" items="${subjectList}">
		<input type="radio" name="subject" value="${subj}"> ${subj} </br>
	</c:forEach>
	--%>
	Hello, ${cust.name} <hr/>
	<sf:form action="books">
		<sf:radiobuttons path="subject" items="${subjectList}" delimiter="<br/>"/>
		<br/>
		<input type="submit" value="Show Books"/> 
		<a href="showcart">Show Cart</a>
	</sf:form>
</body>
</html>


