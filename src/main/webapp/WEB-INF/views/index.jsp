<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
</head>
<body>
	<h3><s:message code="app.title"/></h3>
	<sf:form modelAttribute="cred" action="signin">
		<table>
			<tr>
				<td>
					<s:message code="email.label"/>:
				</td>
				<td>
					<sf:input path="email"/>
				</td>
				<td>
				</td>
			</tr>
			<tr>
				<td>
					<s:message code="password.label"/>:
				</td>
				<td>
					<sf:password path="password"/>
				</td>
				<td>
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" value='<s:message code="signin.label"/>'/>
				</td>
				<td>
					<a href="signup"><s:message code="signup.label"/></a>
				</td>
				<td></td>
			</tr>
		</table>
	</sf:form>
</body>
</html>