package sh.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "customers")
public class Customer {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "id")
	private int custId;
	@NotBlank(message = "*")
	@Column
	private String name;
	@NotBlank
	@Column
	private String address;
	@NotBlank
	@Size(min = 4)
	@Column
	private String password;
	@NotBlank
	@Pattern(regexp = "^[0-9]{10}$")
	@Column
	private String mobile;
	@NotBlank
	@Email
	@Column
	private String email;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@Temporal(TemporalType.DATE)
	@Column
	private Date birth;
	
	public Customer() {
		this.birth = new Date();
	}

	public Customer(int custId, String name, String address, String password, String mobile, String email, Date birth) {
		this.custId = custId;
		this.name = name;
		this.address = address;
		this.password = password;
		this.mobile = mobile;
		this.email = email;
		this.birth = birth;
	}

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}

	@Override
	public String toString() {
		return "Customer [custId=" + custId + ", name=" + name + ", address=" + address + ", password=" + password
				+ ", mobile=" + mobile + ", email=" + email + ", birth=" + birth + "]";
	}
}



