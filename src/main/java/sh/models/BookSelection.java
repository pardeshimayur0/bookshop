package sh.models;

public class BookSelection {
	private String[] book;
	public BookSelection() {
		book = new String[0];
	}
	public String[] getBook() {
		return book;
	}
	public void setBook(String[] book) {
		this.book = book;
	}
}
