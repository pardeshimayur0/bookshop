package sh.models;

public class SubjectSelection {
	private String subject;
	
	public SubjectSelection() {
		subject = "";
	}

	public SubjectSelection(String subject) {
		this.subject = subject;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	
}
