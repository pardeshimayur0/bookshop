package sh.services;

import sh.entities.Customer;

public interface LoginService {
	Customer findByEmail(String email);
	Customer authenticateCustomer(String email, String password);
	void saveCustomer(Customer c);
}
