package sh.services;

import java.util.List;

import sh.entities.Book;

public interface BookService {

	List<String> findSubjects();

	List<Book> findBySubject(String subject);

	Book findById(int id);

	void addBook(Book b);

	void updateBook(Book b);

	void deleteBook(Book b);

	List<Book> getAllBooks();

}