package sh.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sh.daos.LoginDao;
import sh.entities.Customer;

@Service
public class LoginServiceImpl implements LoginService {
	@Autowired
	private LoginDao loginDao;
	
	@Transactional
	public Customer findByEmail(String email) {
		return loginDao.findByEmail(email);
	}
	
	@Transactional
	public Customer authenticateCustomer(String email, String password) {
		Customer dbCust = this.findByEmail(email);
		if(dbCust != null && dbCust.getPassword().equals(password))
			return dbCust;
		return null;
	}
	
	@Transactional
	public void saveCustomer(Customer c) {
		loginDao.saveCustomer(c);
	}
}
