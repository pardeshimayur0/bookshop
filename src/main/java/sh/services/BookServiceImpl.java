package sh.services;

import java.util.List;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sh.daos.BookDao;
import sh.entities.Book;

@Service
public class BookServiceImpl implements BookService {
	@Autowired
	private BookDao bookDao;

	@Transactional
	public List<Book> findBySubject(String subject) {
		return bookDao.findBySubject(subject);
	}
	@Transactional
	public List<Book> getAllBooks() {
		return bookDao.getAllBooks();
	}
	
	@Transactional
	public List<String> findSubjects() {
		return bookDao.findSubjects();
	}
	
	@Transactional
	public Book findById(int id) {
		return bookDao.findById(id);
	}
	
	@Transactional
	public void addBook(Book b) {
		bookDao.addBook(b);
	}

	@Transactional
	public void updateBook(Book b) {
		bookDao.updateBook(b);
	}

	@Transactional
	public void deleteBook(Book b) {
		bookDao.deleteBook(b);
	}

	@Transactional
	public void deleteById(int id) {
		Book b = this.findById(id);
		if(b != null)
			this.deleteBook(b);
	}
}
