package sh.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import sh.entities.Customer;
import sh.models.Credentials;
import sh.services.LoginService;

@Controller
public class LoginControllerImpl {
	@Autowired
	private LoginService loginService;
	@GetMapping("/login")
	//@RequestMapping(value="/login", method = RequestMethod.GET)
	public String index(Model model, Locale locale) {
		System.out.println("Current Locale: " + locale);
		Credentials cred = new Credentials("", "");
		model.addAttribute("cred", cred);
		return "index";
	}
	
	@PostMapping("/signin")
	public String login(Model model, Credentials cred, HttpSession session) {
		Customer dbCust = loginService.authenticateCustomer(
						cred.getEmail(),
						cred.getPassword()
					);
		if(dbCust != null && !dbCust.getName().equalsIgnoreCase("admin")) {
			session.setAttribute("cust", dbCust);
			List<Integer> cart = new ArrayList<Integer>();
			session.setAttribute("cart", cart);
			return "forward:subjects";
			// --> forward current request to @RequestMapping("/subjects") methods
		}else if(dbCust != null && dbCust.getName().equalsIgnoreCase("admin")){
			session.setAttribute("cust", dbCust);
			List<Integer> cart = new ArrayList<Integer>();
			session.setAttribute("cart", cart);
			return "forward:booklist";
		}
		return "failed";
	}
	
	@RequestMapping("/signout")
	public String signOut(HttpSession session) {
		session.invalidate();
		return "logout"; // --> logout.jsp
	}
	
	@GetMapping("/signup")
	public String signUp(Model model) {
		Customer cust = new Customer();
		model.addAttribute("command", cust); // form backing bean
		return "register"; // --> register.jsp
	}
	
	@PostMapping("/signup")
	public String registerUser(@Valid @ModelAttribute("command") Customer cust, BindingResult res, Model model) {
		System.out.println("New Customer: " + cust);
		System.out.println("Binding Result: " + res);
		//validate customer object
		if(res.hasErrors()) {
			return "register";
		}
		//add customer object into database
		
		loginService.saveCustomer(cust);
		System.out.println("Customer Saved: " + cust);		
		model.addAttribute("msg", "Customer registered Successfully.");
		return "register"; // --> register.jsp
	}
}


