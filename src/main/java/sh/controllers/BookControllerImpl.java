package sh.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sh.daos.BookDao;
import sh.entities.Book;
import sh.models.BookSelection;
import sh.models.SubjectSelection;
import sh.services.BookService;

@Controller
public class BookControllerImpl {
	@Autowired
	private BookService bookService;
	
	@RequestMapping("/subjects") // HTTP request is for "subjects" -> handlerAdapter
	public String subjects(Model model) {
		List<String> list = bookService.findSubjects();
		SubjectSelection subSel = new SubjectSelection();
		if(!list.isEmpty())
			subSel.setSubject(list.get(0));
		model.addAttribute("command", subSel);
		model.addAttribute("subjectList", list);
		return "subjects"; // View Name -> viewResolver -> /WEB-INF/views/subjects.jsp
	}
	
	@RequestMapping("/books") // HTTP request is for "books" from subjects.jsp form action -> handlerAdapter
	public String subjectBooks(SubjectSelection subSel, Model model) {
		String subject = subSel.getSubject();
		List<Book> list = bookService.findBySubject(subject);
		model.addAttribute("bookList", list);
		model.addAttribute("command", new BookSelection());
		return "books";  // View Name -> viewResolver -> /WEB-INF/views/books.jsp
	}
	
	@RequestMapping("/addcart")
	public String addCart(BookSelection booksel, HttpSession session) {
		//add book id into session "cart"
		List<Integer> cart = (List<Integer>) session.getAttribute("cart");
		for (String bookId: booksel.getBook()) {
			int id = Integer.parseInt(bookId);
			cart.add(id);
		}
		return "forward:subjects";
	}
	
	@RequestMapping("/showcart")
	public String showCart(HttpSession session, Model model) {
		List<Integer> cart = (List<Integer>) session.getAttribute("cart");
		List<Book> list = new ArrayList<Book>();
		for (int id : cart) {
			Book b = bookService.findById(id);
			list.add(b);
		}
		model.addAttribute("bookList", list);
		return "cart"; // -->viewResolver-->/WEB-INF/views/cart.jsp
	}
	
	@RequestMapping("/details")
	public String bookDetails(@RequestParam("id") int id, Model model) {
		Book b = bookService.findById(id);
		model.addAttribute("b", b);
		return "details"; // --> details.jsp
	}
	
	@RequestMapping("/booklist")
	public String bookList(Model model) {
		List<Book> list = bookService.getAllBooks();
		model.addAttribute("bookList", list);
		//model.addAttribute("command", new BookSelection());
		return "booklist";  // View Name -> viewResolver -> /WEB-INF/views/books.jsp
	}
	
	@RequestMapping("/delete")
	public String deletebook(@RequestParam("id") int id, Model model) {
		Book b = bookService.findById(id);
		bookService.deleteBook(b);
		return "forward:booklist"; // --> details.jsp
	}
	
	@RequestMapping("/edit")
	public String editbook(@RequestParam("id") int id, Model model) {
		Book b = bookService.findById(id);
		model.addAttribute("book", b);
		return "editbook"; // --> details.jsp
	}
	
	@RequestMapping("/add")
	public String addbook(Book book, Model model) {
		//bookService.addBook(book);
		model.addAttribute("book", new Book());
		return "addbook"; // --> details.jsp
	}
	
	@RequestMapping("/addbook")
	public String editbook(Book book, Model model) {
		bookService.addBook(book);
		//model.addAttribute("book", b);
		return "forward:booklist"; // --> details.jsp
	}
	
	@RequestMapping("/updatebook")
	public String updateBook(Book book, Model model) {
		bookService.updateBook(book);
		//model.addAttribute("book", new Book());
		return "forward:booklist";
	}
}
