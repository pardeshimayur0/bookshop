package sh.daos;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sh.entities.Book;

@Repository
public class BookDaoImpl implements BookDao {
	@Autowired
	private SessionFactory factory;

	@Override
	public List<String> findSubjects() {
		String hql = "select distinct b.subject from Book b";
		Session session = factory.getCurrentSession();
		Query<String> q = session.createQuery(hql);
		return q.getResultList();
	}
	
	@Override
	public List<Book> findBySubject(String subject) {
		String hql = "from Book b where b.subject=:p_subject";
		Session session = factory.getCurrentSession();
		Query<Book> q = session.createQuery(hql);
		q.setParameter("p_subject", subject);
		return q.getResultList();
	}
	
	@Override
	public List<Book> getAllBooks() {
		String hql = "from Book b";
		Session session = factory.getCurrentSession();
		Query<Book> q = session.createQuery(hql);
		return q.getResultList();
	}
	
	@Override
	public Book findById(int id) {
		Session session = factory.getCurrentSession();
		Book cust = session.find(Book.class, id);
		return cust;
	}
	
	@Override
	public void addBook(Book b) {
		Session session = factory.getCurrentSession();
		session.persist(b);
	}

	@Override
	public void updateBook(Book b) {
		Session session = factory.getCurrentSession();
		session.merge(b);
	}

	@Override
	public void deleteBook(Book b) {
		Session session = factory.getCurrentSession();
		session.remove(b);
	}
}





