package sh.daos;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import sh.entities.Customer;

@Repository
public class LoginDaoImpl implements LoginDao {
	@Autowired
	private SessionFactory factory;

	@Override
	public Customer findByEmail(String email) {
		Session session = factory.getCurrentSession();
		String hql = "from Customer c where c.email=:p_email";
		Query<Customer> q = session.createQuery(hql);
		q.setParameter("p_email", email);
		return q.uniqueResult();
	}
	
	@Override
	public void saveCustomer(Customer c) {
		Session session = factory.getCurrentSession();
		session.persist(c);
	}
}
