package sh.daos;

import sh.entities.Customer;

public interface LoginDao {

	Customer findByEmail(String email);

	void saveCustomer(Customer c);

}