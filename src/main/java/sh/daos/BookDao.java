package sh.daos;

import java.util.List;

import sh.entities.Book;

public interface BookDao {

	List<String> findSubjects();

	List<Book> findBySubject(String subject);

	Book findById(int id);

	void addBook(Book b);

	void updateBook(Book b);

	void deleteBook(Book b);

	List<Book> getAllBooks();

}